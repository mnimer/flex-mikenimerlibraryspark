package com.mikenimer.components.toast
{
	import flash.events.TimerEvent;
	import flash.system.System;
	import flash.utils.Timer;
	
	import mx.core.FlexGlobals;

    import mx.core.UIComponent;

    import spark.components.Application;
	import spark.components.BorderContainer;
	import spark.components.Group;
	import spark.components.Label;
	import spark.layouts.VerticalLayout;

public class Toast extends UIComponent //extends BorderContainer
{
	private var context:*;
	private var textToDisplay:String;
	private var timeOut:Number;
	private var toastWidth:Number;
	private var toastHeight:Number;
	private var toastColor:String;
	private var toastX:Number;
	private var toastY:Number;
	
	
	public function Toast(context:*, textToDisplay:String, timeOut:Number = 3000, width:Number = 290, height:Number = 40, color:String = '#454545', x:Number = NaN, y:Number = NaN):void
	{
		super();
		this.context = context;
		this.textToDisplay = textToDisplay;
		this.timeOut = timeOut;
		this.toastWidth = width;
		this.toastHeight = height;
		this.toastColor = color;
		this.toastX = x;
		this.toastY = y;
		
	}

	
	public function show():void
	{
		// TIMER TO 'REMOVE' TOAST AFTER THE SPECIFIED 'TIMEOUT'
		var timer:Timer = new Timer(timeOut, 1);
		timer.addEventListener(TimerEvent.TIMER_COMPLETE
			, function(event:TimerEvent):void
			{
				context.removeElementAt(context.numElements - 1);
			});
		timer.start();
		
		createUI(textToDisplay, toastColor, toastWidth, toastHeight);
		
		invalidateDisplayList();
		invalidateSize();
		invalidateProperties();
		validateNow();
		
		// TOAST POSITION
		positionToast(context);
	}
	
	/**
	 *  CREATE TOAST UI COMPONENTS
	 */
	private function createUI(textToDisplay:String, color:String, width:Number, height:Number):void
	{
		selfProperties(color, width, height);
		addText(textToDisplay);
	}
	
	/**
	 * ASSIGNING ATTRIBUTES TO THE BASE CONTAINER
	 */
	private function selfProperties(color:String, width:Number, height:Number):void
	{
		this.width=width;
		this.height=height;
		this.setStyle('backgroundAlpha','0.75');
		this.setStyle('backgroundColor',color);
		this.setStyle('borderColor','#242424');
		this.setStyle('borderWeight','2');
		this.setStyle('cornerRadius','7');
		
		var vl:VerticalLayout = new VerticalLayout();
		vl.horizontalAlign = "center";
		vl.verticalAlign = "middle";
		//this.layout = vl;
	}
	
	/**
	 *  CREATE LABEL
	 */
	private function addText(textToDisplay:String):void
	{
		var lbl:Label = new Label();
		lbl.setStyle('color','#EDEDED');
		lbl.setStyle('fontSize','18');
		lbl.text = textToDisplay;
		lbl.setStyle('textAlign', 'center');
		lbl.width = this.width;
		
		//this.addElement(lbl);
	}
	
	/**
	 *  POSITIONING THE TOAST
	 */
	private function positionToast(context:*):void
	{
		// POSITION TOAST
		if(Number(x) && Number(y))
		{
			this.x = x;
			this.y = y;
		}
		else
		{
			if(Number(x) && !Number(y))
			{
				throw new Error("Toast : 'Y' attribute also needs to be specified");
			}
			
			
			var application:Application = Application(FlexGlobals.topLevelApplication);
			
			this.x = ((application.width / 2) - (this.width / 2));
			this.y = (application.height * 3 / 4) - (this.height / 2);
		}
		
		// IF PREVIOUSLY ADDED COMPONENT IS A TOAST, RE-ORDER TO MANAGE TIMER
		if(context.numElements > 0 && context.getElementAt(context.numElements - 1) is Toast)
		{
			context.addElementAt(this, Math.max(context.numElements - 1, 0));
		}
		else
		{
			context.addElement(this);
		}
	}
}
}